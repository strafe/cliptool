// clang-format off
#include "headers.h"
#include "bsp.h"
#include "main.h"
#include <fstream>
#include <iostream>
#include <string>
// clang-format on

uintptr_t g_ptrLimitOfTempEntities = 0;

#ifndef WIN32
auto g_PageSize = sysconf(_SC_PAGE_SIZE);
#endif

CCollisionBSPData* g_BSPData = nullptr;
IVEngineServer* g_EngineServer = nullptr;
ClipPolyToPlane_t ClipPolyToPlane = nullptr;
PolyFromPlane_t PolyFromPlane = nullptr;
ConMsg_t ConMsg = nullptr;

std::vector<BYTE> g_vecBackup_WriteTempEntities;

CClipToolPlugin g_ClipToolPlugin;

// can you search for the functions for windows? until i make food xd ok
std::string getExePath()
{
#ifndef WIN32
	char result[256];
	size_t count = readlink("/proc/self/exe", result, sizeof(result));
	return std::string(dirname(result), (count > 0) ? count : 0);
#else
	HMODULE hModule = GetModuleHandleW(NULL);
	char path[MAX_PATH];
	// This will get the path of the exe yeah but with itsname included i think, we gotta change after.
	GetModuleFileName(hModule, path, MAX_PATH);
	GetFullPathName(path, MAX_PATH, path, nullptr);

	return std::string(path);
#endif
}

void CClipToolPlugin::Unload() 
{	
	ConMsg("Unloading %s\n", GetPluginDescription()); 

#ifndef WIN32
	// Align address to make mprotect working, this isn't an issue under windows tbh.
	auto alignedAddress = (g_ptrLimitOfTempEntities & ~(g_PageSize - 1));

	// Set protection flags so we can write into the memory page.
	if (mprotect(reinterpret_cast<void*>(alignedAddress), g_PageSize, PROT_EXEC | PROT_WRITE | PROT_READ) != -1)
#else
	DWORD dwOldProt;
	if(VirtualProtect(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 4, PAGE_EXECUTE_READWRITE, &dwOldProt))
#endif
	{
#ifndef WIN32
		ConMsg("Changed memory protection flags at: 0x%X for size %i\n", alignedAddress, g_PageSize);
#else
		ConMsg("Changed memory protection flags at: 0x%X for size %i\n", g_ptrLimitOfTempEntities, 4);
#endif
		// if(a6 <= 255)
		//.text:000D9900 v10 = a6; → cmovle  ecx, [ebp+arg_14]
		memcpy(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), g_vecBackup_WriteTempEntities.data(), g_vecBackup_WriteTempEntities.size());

#ifndef WIN32
		// Reset protection flags.
		if(mprotect(reinterpret_cast<void*>(alignedAddress), g_PageSize, PROT_EXEC | PROT_READ) != -1)
#else
		if(VirtualProtect(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 4, dwOldProt, &dwOldProt))
#endif
		{
			ConMsg("Successfully set old instructions at 0x%X\n", g_ptrLimitOfTempEntities);
		}
	}
}

bool CClipToolPlugin::Load(CreateInterfaceFn interfaceFactory, CreateInterfaceFn gameServerFactory)
{
	printf("Loading %s...\n", GetPluginDescription());

#ifndef WIN32
	auto lm_Server = reinterpret_cast<link_map*>(dlopen("cstrike/bin/server_srv.so", RTLD_NOW));
	auto lm_Engine = reinterpret_cast<link_map*>(dlopen("bin/engine_srv.so", RTLD_NOW));
	auto lm_Tier0 = reinterpret_cast<link_map*>(dlopen("bin/libtier0_srv.so", RTLD_NOW));

	if (lm_Server == nullptr || lm_Engine == nullptr || lm_Tier0 == nullptr)
	{
		printf("Failed to get libraries...\n");
		return false;
	}

	ConMsg = reinterpret_cast<ConMsg_t>(dlsym(lm_Engine, "_Z6ConMsgPKcz"));
	printf("ConMsg %p\n", ConMsg);

	if (ConMsg == nullptr)
	{
		printf("Failed get ConMsg\n");
		return false;
	}

	g_BSPData = reinterpret_cast<CCollisionBSPData*>(lm_Engine->l_addr + 0x3112A0);
	ConMsg("g_BSPData %p\n", g_BSPData);

	PolyFromPlane = reinterpret_cast<PolyFromPlane_t>(lm_Engine->l_addr + 0x2103B0);
	ConMsg("PolyFromPlane %p\n", PolyFromPlane);

	ClipPolyToPlane = reinterpret_cast<ClipPolyToPlane_t>(lm_Engine->l_addr + 0x210670);
	ConMsg("ClipPolyToPlane %p\n", ClipPolyToPlane);

	g_ptrLimitOfTempEntities = lm_Engine->l_addr + 0xD9900;

#else

	HMODULE server = GetModuleHandle("cstrike/bin/server.dll");
	HMODULE engine = GetModuleHandle("bin/engine.dll");
	HMODULE tier0 = GetModuleHandle("bin/tier0.dll");

	if (server == nullptr || engine == nullptr || tier0 == nullptr)
	{
		printf("[windows] Failed to get libraries...\n");
		return false;
	}

	ConMsg = reinterpret_cast<ConMsg_t>(GetProcAddress(tier0, "Msg"));

	if (ConMsg == nullptr)
	{
		printf("[windows] failed to get ConMsg\n");
		return false;
	}

	// ref: CM_Vis:  buffer not big enough (%i but need %i)
	g_BSPData = reinterpret_cast<CCollisionBSPData*>(reinterpret_cast<uintptr_t>(engine) + 0x6275B8);
	PolyFromPlane = reinterpret_cast<PolyFromPlane_t>(reinterpret_cast<uintptr_t>(engine) + 0x2414D0);
	ClipPolyToPlane = reinterpret_cast<ClipPolyToPlane_t>(reinterpret_cast<uintptr_t>(engine) + 0x2403D0);
	g_ptrLimitOfTempEntities = reinterpret_cast<uintptr_t>(engine) + 0x15F235;

#endif
	g_vecBackup_WriteTempEntities.resize(4);
	memcpy(g_vecBackup_WriteTempEntities.data(), reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 4);

#ifndef WIN32
	// Align address to make mprotect working, this isn't an issue under windows tbh.
	auto alignedAddress = (g_ptrLimitOfTempEntities & ~(g_PageSize - 1));

	// Set protection flags so we can write into the memory page.
	if (mprotect(reinterpret_cast<void*>(alignedAddress), g_PageSize, PROT_EXEC | PROT_WRITE | PROT_READ) != -1)
#else
	DWORD dwOldProt;
	if(VirtualProtect(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 4, PAGE_EXECUTE_READWRITE, &dwOldProt))
#endif
	{
#ifndef WIN32
		ConMsg("Changed memory protection flags at: 0x%X for size %i\n", alignedAddress, g_PageSize);
#else
		ConMsg("Changed memory protection flags at: 0x%X for size %i\n", g_ptrLimitOfTempEntities, 4);
#endif

		// if(a6 <= 255)
		//.text:000D9900 v10 = a6; → cmovle  ecx, [ebp+arg_14]
		memset(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 0x90, 4);

#ifndef WIN32
		// Reset protection flags.
		if(mprotect(reinterpret_cast<void*>(alignedAddress), g_PageSize, PROT_EXEC | PROT_READ) != -1)
#else
		if(VirtualProtect(reinterpret_cast<void*>(g_ptrLimitOfTempEntities), 4, dwOldProt, &dwOldProt))
#endif
		{
			ConMsg("Successfully set new instructions at 0x%X\n", g_ptrLimitOfTempEntities);
		}
	}

	g_EngineServer = reinterpret_cast<IVEngineServer*>(interfaceFactory(INTERFACEVERSION_VENGINESERVER, 0));

	return true;
}

/**
 * Extracts brushes and transforms their vertices into an array of drawable lines
 */
void transformForRender(const leafvis_t* const leafvis, std::vector<renderset_t>* const render_array)
{
	if (leafvis == nullptr)
		return;
	if (render_array == nullptr)
		return;

	int cursor = 0;
	
	for (size_t polygon = 0; polygon < leafvis->polyVertCount.size(); ++polygon)
	{
		if (leafvis->polyVertCount[polygon] >= 3) // polygon has 3 or more vertices
		{
			// prepare a set of lines to render
			renderset_t renderset;
			renderset.contents = leafvis->contents;

			// add lines
			for (int start = 0; start < leafvis->polyVertCount[polygon]; ++start)
			{
				int end = cursor + ((start + 1) % leafvis->polyVertCount[polygon]);

				line_t line;
				line.start = leafvis->verts[cursor + start];
				line.end = leafvis->verts[end];
				renderset.lines.push_back(line);
			}
			render_array->push_back(renderset);
		}
		cursor += leafvis->polyVertCount[polygon];
	}
}

/*
 * Returns the total number of lines
 */
size_t getTotalLines(const std::vector<renderset_t>* const render_array)
{
	if (render_array == nullptr)
		return 0;

	size_t sum = 0;

	for (const renderset_t& renderset : *render_array)
	{
		sum += renderset.lines.size();
	}

	return sum;
}

/*
 * Returns the total number of vertices cached
 */
size_t getTotalVerts(const std::vector<leafvis_t>* const leafvis_array)
{
	if (leafvis_array == nullptr)
		return 0;

	size_t sum = 0;

	for (const leafvis_t& leafvis : *leafvis_array)
	{
		sum += leafvis.verts.size();
	}

	return sum;
}

/*
 * Returns the total number of polygons cached
 */
size_t getTotalPolygons(const std::vector<leafvis_t>* const leafvis_array)
{
	if (leafvis_array == nullptr)
		return 0;

	size_t sum = 0;

	for (const leafvis_t& leafvis : *leafvis_array)
	{
		sum += leafvis.polyVertCount.size();
	}

	return sum;
}

/*
 * Writes our transformed render array to the file system for the sp plugin to read in later
 */
void writeRenderArrayToFile(renderset_t* renderset, std::ofstream& streamFile)
{
	if (renderset == nullptr)
		return;

	int numlines = renderset->lines.size();
	streamFile.write(reinterpret_cast<char*>(&numlines), sizeof(int));

	for (line_t& line : renderset->lines)
	{
		streamFile.write(reinterpret_cast<char*>(&line), sizeof(line_t));
	}
	streamFile.write(reinterpret_cast<char*>(&renderset->contents), sizeof(int));
}

void CClipToolPlugin::LevelInit(const char* pMapName)
{
	const char* clipPath = "./cstrike/addons/cliptool/";

	parseClipBrushes();

	if (g_ClipVis.empty())
	{
		ConMsg("Map(%s) doesn't have any clip brushes %s\n", clipPath);
		return;
	}

	ConMsg("[cliptool] Parsed map: %s with %i leafvis, %i polygons, %i vertices\n", 
		pMapName, 
		g_ClipVis.size(), 
		getTotalPolygons(&g_ClipVis),
		getTotalVerts(&g_ClipVis));

#ifndef WIN32
	mkdir(clipPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#else
	CreateDirectory(clipPath, NULL);
#endif

	ConMsg("[cliptool] Transforming vertices into lines for render...\n");
	std::vector<renderset_t> render_array;

	for (auto &&i : g_ClipVis)
	{
		transformForRender(&i, &render_array);
	}
	ConMsg("[cliptool] Done. Total lines: %i\n", getTotalLines(&render_array));

	std::string filePath = std::string(std::string(clipPath) + pMapName + ".dat");
	std::ofstream streamFile(filePath, std::ifstream::binary);

	if (!streamFile.is_open())
	{
		ConMsg("[cliptool] Couldn't open file %s\n", filePath.c_str());
		return;
	}

	int size = render_array.size();
	streamFile.write(reinterpret_cast<char*>(&size), sizeof(int));

	for (auto &&i : render_array)
		writeRenderArrayToFile(&i, streamFile);

	ConMsg("[cliptool] Wrote to file %s\n", filePath.c_str());

	streamFile.close();
}

#ifdef WIN32
// Avoid linking issues
BOOL WINAPI DllMain(
	HINSTANCE hinstDLL,  // handle to DLL module
	DWORD fdwReason,     // reason for calling function
	LPVOID lpReserved)  // reserved
{
	return TRUE;
}
#endif

PLUGIN_RESULT CClipToolPlugin::ClientCommand(edict_t* pEntity, const CCommand& args) { return PLUGIN_CONTINUE; }

EXPOSE_SINGLE_INTERFACE_GLOBALVAR(CClipToolPlugin, IServerPluginCallbacks, INTERFACEVERSION_ISERVERPLUGINCALLBACKS, g_ClipToolPlugin);
