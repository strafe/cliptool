// clang-format off
#include "headers.h"
#include "bsp.h"
#include "main.h"
// clang-format on

InterfaceReg* InterfaceReg::s_pInterfaceRegs = nullptr;

InterfaceReg::InterfaceReg( InstantiateInterfaceFn fn, const char* pName ) : m_pName( pName )
{
	m_CreateFn = fn;
	m_pNext = s_pInterfaceRegs;
	s_pInterfaceRegs = this;
}

void* CreateInterfaceInternal( const char* pName, int* pReturnCode )
{
	InterfaceReg* pCur;

	for ( pCur = InterfaceReg::s_pInterfaceRegs; pCur; pCur = pCur->m_pNext )
	{
		if ( strcmp( pCur->m_pName, pName ) == 0 )
		{
			if ( pReturnCode )
			{
				*pReturnCode = IFACE_OK;
			}
			return pCur->m_CreateFn();
		}
	}

	if ( pReturnCode )
	{
		*pReturnCode = IFACE_FAILED;
	}

	return NULL;
}

DLL_EXPORT void* CreateInterface( const char* pName, int* pReturnCode ) { return CreateInterfaceInternal( pName, pReturnCode ); }
