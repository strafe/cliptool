#pragma once
#ifndef BSP_H
#	define BSP_H
#	include "vector.h"

// contents flags are seperate bits
// a given brush can contribute multiple content bits
// multiple brushes can be in a single leaf

// these definitions also need to be in q_shared.h!

// lower bits are stronger, and will eat weaker brushes completely
#define	CONTENTS_EMPTY			0		// No contents

#define	CONTENTS_SOLID			0x1		// an eye is never valid in a solid
#define	CONTENTS_WINDOW			0x2		// translucent, but not watery (glass)
#define	CONTENTS_AUX			0x4
#define	CONTENTS_GRATE			0x8		// alpha-tested "grate" textures.  Bullets/sight pass through, but solids don't
#define	CONTENTS_SLIME			0x10
#define	CONTENTS_WATER			0x20
#define	CONTENTS_BLOCKLOS		0x40	// block AI line of sight
#define CONTENTS_OPAQUE			0x80	// things that cannot be seen through (may be non-solid though)
#define	LAST_VISIBLE_CONTENTS	0x80

#define ALL_VISIBLE_CONTENTS (LAST_VISIBLE_CONTENTS | (LAST_VISIBLE_CONTENTS-1))

#define CONTENTS_TESTFOGVOLUME	0x100
#define CONTENTS_UNUSED			0x200	

// unused 
// NOTE: If it's visible, grab from the top + update LAST_VISIBLE_CONTENTS
// if not visible, then grab from the bottom.
#define CONTENTS_UNUSED6		0x400

#define CONTENTS_TEAM1			0x800	// per team contents used to differentiate collisions 
#define CONTENTS_TEAM2			0x1000	// between players and objects on different teams

// ignore CONTENTS_OPAQUE on surfaces that have SURF_NODRAW
#define CONTENTS_IGNORE_NODRAW_OPAQUE	0x2000

// hits entities which are MOVETYPE_PUSH (doors, plats, etc.)
#define CONTENTS_MOVEABLE		0x4000

// remaining contents are non-visible, and don't eat brushes
#define	CONTENTS_AREAPORTAL		0x8000

#define	CONTENTS_PLAYERCLIP		0x10000
#define	CONTENTS_MONSTERCLIP	0x20000

// currents can be added to any other contents, and may be mixed
#define	CONTENTS_CURRENT_0		0x40000
#define	CONTENTS_CURRENT_90		0x80000
#define	CONTENTS_CURRENT_180	0x100000
#define	CONTENTS_CURRENT_270	0x200000
#define	CONTENTS_CURRENT_UP		0x400000
#define	CONTENTS_CURRENT_DOWN	0x800000

#define	CONTENTS_ORIGIN			0x1000000	// removed before bsping an entity

#define	CONTENTS_MONSTER		0x2000000	// should never be on a brush, only in game
#define	CONTENTS_DEBRIS			0x4000000
#define	CONTENTS_DETAIL			0x8000000	// brushes to be added after vis leafs
#define	CONTENTS_TRANSLUCENT	0x10000000	// auto set if any surface has trans
#define	CONTENTS_LADDER			0x20000000
#define CONTENTS_HITBOX			0x40000000	// use accurate hitboxes on trace


// NOTE: These are stored in a short in the engine now.  Don't use more than 16 bits
#define	SURF_LIGHT		0x0001		// value will hold the light strength
#define	SURF_SKY2D		0x0002		// don't draw, indicates we should skylight + draw 2d sky but not draw the 3D skybox
#define	SURF_SKY		0x0004		// don't draw, but add to skybox
#define	SURF_WARP		0x0008		// turbulent water warp
#define	SURF_TRANS		0x0010
#define SURF_NOPORTAL	0x0020	// the surface can not have a portal placed on it
#define	SURF_TRIGGER	0x0040	// FIXME: This is an xbox hack to work around elimination of trigger surfaces, which breaks occluders
#define	SURF_NODRAW		0x0080	// don't bother referencing the texture

#define	SURF_HINT		0x0100	// make a primary bsp splitter

#define	SURF_SKIP		0x0200	// completely ignore, allowing non-closed brushes
#define SURF_NOLIGHT	0x0400	// Don't calculate light
#define SURF_BUMPLIGHT	0x0800	// calculate three lightmaps for the surface for bumpmapping
#define SURF_NOSHADOWS	0x1000	// Don't receive shadows
#define SURF_NODECALS	0x2000	// Don't receive decals
#define SURF_NOCHOP		0x4000	// Don't subdivide patches on this surface 
#define SURF_HITBOX		0x8000	// surface is part of a hitbox



// -----------------------------------------------------
// spatial content masks - used for spatial queries (traceline,etc.)
// -----------------------------------------------------
#define	MASK_ALL					(0xFFFFFFFF)
// everything that is normally solid
#define	MASK_SOLID					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// everything that blocks player movement
#define	MASK_PLAYERSOLID			(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_PLAYERCLIP|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// blocks npc movement
#define	MASK_NPCSOLID				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTERCLIP|CONTENTS_WINDOW|CONTENTS_MONSTER|CONTENTS_GRATE)
// water physics in these contents
#define	MASK_WATER					(CONTENTS_WATER|CONTENTS_MOVEABLE|CONTENTS_SLIME)
// everything that blocks lighting
#define	MASK_OPAQUE					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_OPAQUE)
// everything that blocks lighting, but with monsters added.
#define MASK_OPAQUE_AND_NPCS		(MASK_OPAQUE|CONTENTS_MONSTER)
// everything that blocks line of sight for AI
#define MASK_BLOCKLOS				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_BLOCKLOS)
// everything that blocks line of sight for AI plus NPCs
#define MASK_BLOCKLOS_AND_NPCS		(MASK_BLOCKLOS|CONTENTS_MONSTER)
// everything that blocks line of sight for players
#define	MASK_VISIBLE					(MASK_OPAQUE|CONTENTS_IGNORE_NODRAW_OPAQUE)
// everything that blocks line of sight for players, but with monsters added.
#define MASK_VISIBLE_AND_NPCS		(MASK_OPAQUE_AND_NPCS|CONTENTS_IGNORE_NODRAW_OPAQUE)
// bullets see these as solid
#define	MASK_SHOT					(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTER|CONTENTS_WINDOW|CONTENTS_DEBRIS|CONTENTS_HITBOX)
// non-raycasted weapons see this as solid (includes grates)
#define MASK_SHOT_HULL				(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_MONSTER|CONTENTS_WINDOW|CONTENTS_DEBRIS|CONTENTS_GRATE)
// hits solids (not grates) and passes through everything else
#define MASK_SHOT_PORTAL			(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTER)
// everything normally solid, except monsters (world+brush only)
#define MASK_SOLID_BRUSHONLY		(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_GRATE)
// everything normally solid for player movement, except monsters (world+brush only)
#define MASK_PLAYERSOLID_BRUSHONLY	(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_PLAYERCLIP|CONTENTS_GRATE)
// everything normally solid for npc movement, except monsters (world+brush only)
#define MASK_NPCSOLID_BRUSHONLY		(CONTENTS_SOLID|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_MONSTERCLIP|CONTENTS_GRATE)
// just the world, used for route rebuilding
#define MASK_NPCWORLDSTATIC			(CONTENTS_SOLID|CONTENTS_WINDOW|CONTENTS_MONSTERCLIP|CONTENTS_GRATE)
// These are things that can split areaportals
#define MASK_SPLITAREAPORTAL		(CONTENTS_WATER|CONTENTS_SLIME)

// UNDONE: This is untested, any moving water
#define MASK_CURRENT				(CONTENTS_CURRENT_0|CONTENTS_CURRENT_90|CONTENTS_CURRENT_180|CONTENTS_CURRENT_270|CONTENTS_CURRENT_UP|CONTENTS_CURRENT_DOWN)

// everything that blocks corpse movement
// UNDONE: Not used yet / may be deleted
#define	MASK_DEADSOLID				(CONTENTS_SOLID|CONTENTS_PLAYERCLIP|CONTENTS_WINDOW|CONTENTS_GRATE)

typedef unsigned char byte;

class CPhysCollide;

struct vcollide_t
{
	unsigned short solidCount : 15;
	unsigned short isPacked : 1;
	unsigned short descSize;
	// VPhysicsSolids
	CPhysCollide** solids;
	char* pKeyValues;
};

struct cmodel_t
{
	Vector mins, maxs;
	Vector origin;  // for sounds or lights
	int headnode;

	vcollide_t vcollisionData;
};

struct csurface_t
{
	const char* name;
	short surfaceProps;
	unsigned short flags;  // BUGBUG: These are declared per surface, not per material, but this database is
						   // per-material now
};

#	define DVIS_PVS 0
#	define DVIS_PAS 1
struct dvis_t
{
	int numclusters;
	int bitofs[ 8 ][ 2 ];  // bitofs[numclusters][2]
};

struct dareaportal_t
{
	unsigned short m_PortalKey;  // Entities have a key called portalnumber (and in vbsp a variable
								 // called areaportalnum) which is used
								 // to bind them to the area portals by comparing with this value.

	unsigned short otherarea;  // The area this portal looks into.

	unsigned short m_FirstClipPortalVert;  // Portal geometry.
	unsigned short m_nClipPortalVerts;

	int planenum;
};

//-----------------------------------------------------------------------------
// A ray...
//-----------------------------------------------------------------------------

struct Ray_t
{
	VectorAligned m_Start;  // starting point, centered within the extents
	VectorAligned m_Delta;  // direction + length of the ray
	VectorAligned m_StartOffset;  // Add this to m_Start to get the actual ray start
	VectorAligned m_Extents;  // Describes an axis aligned box extruded along a ray
	bool m_IsRay;  // are the extents zero?
	bool m_IsSwept;  // is delta != 0?

	void Init( Vector const& start, Vector const& end )
	{
		VectorSubtract( end, start, m_Delta );

		m_IsSwept = ( m_Delta.LengthSqr() != 0 );

		VectorClear( m_Extents );
		m_IsRay = true;

		// Offset m_Start to be in the center of the box...
		VectorClear( m_StartOffset );
		VectorCopy( start, m_Start );
	}

	void Init( Vector const& start, Vector const& end, Vector const& mins, Vector const& maxs )
	{
		VectorSubtract( end, start, m_Delta );

		m_IsSwept = ( m_Delta.LengthSqr() != 0 );

		VectorSubtract( maxs, mins, m_Extents );
		m_Extents *= 0.5f;
		m_IsRay = ( m_Extents.LengthSqr() < 1e-6 );

		// Offset m_Start to be in the center of the box...
		VectorAdd( mins, maxs, m_StartOffset );
		m_StartOffset *= 0.5f;
		VectorAdd( start, m_StartOffset, m_Start );
		m_StartOffset *= -1.0f;
	}

	// compute inverse delta
	Vector InvDelta() const
	{
		Vector vecInvDelta;
		for ( int iAxis = 0; iAxis < 3; ++iAxis )
		{
			if ( m_Delta[ iAxis ] != 0.0f )
			{
				vecInvDelta[ iAxis ] = 1.0f / m_Delta[ iAxis ];
			}
			else
			{
				vecInvDelta[ iAxis ] = FLT_MAX;
			}
		}
		return vecInvDelta;
	}

 private:
};

struct cplane_t
{
	Vector normal;
	float dist;
	byte type;  // for fast side tests
	byte signbits;  // signx + (signy<<1) + (signz<<1)
	byte pad[ 2 ];

	cplane_t() {}
};


struct custom_cplane_t : cplane_t
{
	int contents;
};

struct cbrushside_t
{
	cplane_t* plane;
	unsigned short surfaceIndex;
	unsigned short bBevel;  // is the side a bevel plane?
};

#	define NUMSIDES_BOXBRUSH 0xFFFF

struct cbrush_t
{
	int contents;
	unsigned short numsides;
	unsigned short firstbrushside;

	inline int GetBox() const { return firstbrushside; }
	inline void SetBox( int boxID )
	{
		numsides = NUMSIDES_BOXBRUSH;
		firstbrushside = boxID;
	}
	inline bool IsBox() const { return numsides == NUMSIDES_BOXBRUSH ? true : false; }
};

// 48-bytes, aligned to 16-byte boundary
// this is a brush that is an AABB.  It's encoded this way instead of with 6 brushsides
struct cboxbrush_t
{
	VectorAligned mins;
	VectorAligned maxs;

	unsigned short surfaceIndex[ 6 ];
	unsigned short pad2[ 2 ];
};

struct cleaf_t
{
	int contents;
	short cluster;
	short area : 9;
	short flags : 7;
	unsigned short firstleafbrush;
	unsigned short numleafbrushes;
	unsigned short dispListStart;
	unsigned short dispCount;
};

struct carea_t
{
	int numareaportals;
	int firstareaportal;
	int floodnum;  // if two areas have equal floodnums, they are connected
	int floodvalid;
};

struct cnode_t
{
	cplane_t* plane;
	int children[ 2 ];  // negative numbers are leafs
};

template < class T >
class CRangeValidatedArray
{
 public:
	T& operator[]( int i ) { return m_pArray[ i ]; }
	const T& operator[]( int i ) const { return m_pArray[ i ]; }

	T* m_pArray;
};

class CUtlString
{
 public:
	char* m_pString;
};

template < class T >
class CDiscardableArray
{
	int m_nCount;
	CUtlString m_buf;
	char m_pFilename[ 256 ];
	int m_nOffset;
};

#	define MAX_QPATH 96

class CCollisionBSPData
{
 public:
	// This is sort of a hack, but it was a little too painful to do this any other way
	// The goal of this dude is to allow us to override the tree with some
	// other tree (or a subtree)
	cnode_t* map_rootnode;

	char map_name[ MAX_QPATH ];
	static csurface_t nullsurface;

	int numbrushsides;
	CRangeValidatedArray< cbrushside_t > map_brushsides;
	int numboxbrushes;
	CRangeValidatedArray< cboxbrush_t > map_boxbrushes;
	int numplanes;
	CRangeValidatedArray< cplane_t > map_planes;
	int numnodes;
	CRangeValidatedArray< cnode_t > map_nodes;
	int numleafs;  // allow leaf funcs to be called without a map
	CRangeValidatedArray< cleaf_t > map_leafs;
	int emptyleaf, solidleaf;
	int numleafbrushes;
	CRangeValidatedArray< unsigned short > map_leafbrushes;
	int numcmodels;
	CRangeValidatedArray< cmodel_t > map_cmodels;
	int numbrushes;
	CRangeValidatedArray< cbrush_t > map_brushes;
	int numdisplist;
	CRangeValidatedArray< unsigned short > map_dispList;

	// this points to the whole block of memory for vis data, but it is used to
	// reference the header at the top of the block.
	int numvisibility;
	dvis_t* map_vis;

	int numentitychars;
	CDiscardableArray< char > map_entitystring;

	int numareas;
	CRangeValidatedArray< carea_t > map_areas;
	int numareaportals;
	CRangeValidatedArray< dareaportal_t > map_areaportals;
	int numclusters;
	char* map_nullname;
	int numtextures;
	char* map_texturenames;
	CRangeValidatedArray< csurface_t > map_surfaces;
	int floodvalid;
	int numportalopen;
	CRangeValidatedArray< bool > portalopen;

	csurface_t* GetSurfaceAtIndex( unsigned short surfaceIndex );
};

extern CCollisionBSPData* g_BSPData;

struct leafvis_t
{
	leafvis_t()
	{
		leafIndex = 0;
		CCollisionBSPData* pBSP = g_BSPData;
		if ( pBSP )
		{
			numbrushes = pBSP->numbrushes;
			numentitychars = pBSP->numentitychars;
		}
	}

	bool IsValid()
	{
		CCollisionBSPData* pBSP = g_BSPData;
		if ( !pBSP || numbrushes != pBSP->numbrushes || numentitychars != pBSP->numentitychars )
			return false;

		return true;
	}

	std::vector< Vector > verts;
	std::vector< int > polyVertCount;
	//Vector color; // saving memory, adding custom coloring in sourcepawn based on the contents mask
	int numbrushes;
	int numentitychars;
	int leafIndex;
	int contents;
};

typedef int ( *ClipPolyToPlane_t )( Vector*, int, Vector*, Vector const&, float, float );
extern ClipPolyToPlane_t ClipPolyToPlane;
typedef int ( *PolyFromPlane_t )( Vector*, Vector const&, float, float );
extern PolyFromPlane_t PolyFromPlane;

extern std::vector<leafvis_t> g_ClipVis;

extern void parseClipBrushes();

/* Switching data methods */
struct line_t
{
	Vector start;
	Vector end;
};

struct renderset_t // basically each brush
{
	std::vector<line_t> lines;
	int contents;
};

#endif