#pragma once
#ifndef MAIN_DLL
#	define MAIN_DLL
#ifndef WIN32
#	define FORCEINLINE inline
#endif

#ifndef WIN32
#	if __x86_64__ || __ppc64__
#		define MX64
#	else
#		define MX86
#	endif
#endif

#	define InvalidQueryCvarCookie -1

// CHANGE IT WITH THE VERSION OF YOUR GAME! This is currently for CS:GO.
#	define INTERFACEVERSION_ISERVERPLUGINCALLBACKS "ISERVERPLUGINCALLBACKS003"

typedef void(*ConMsg_t)(const char* msg, ...);
extern ConMsg_t ConMsg;

enum
{
	IFACE_OK = 0,
	IFACE_FAILED
};

//
// you will also want to listen for game events via
// IGameEventManager::AddListener()
//

typedef enum
{
	PLUGIN_CONTINUE = 0,  // keep going
	PLUGIN_OVERRIDE,  // run the game dll function but use our return value
					  // instead
					  PLUGIN_STOP,  // don't run the game dll function at all
} PLUGIN_RESULT;

typedef enum
{
	eQueryCvarValueStatus_ValueIntact = 0,  // It got the value fine.
	eQueryCvarValueStatus_CvarNotFound = 1,
	eQueryCvarValueStatus_NotACvar = 2,  // There's a ConCommand, but it's not a
										 // ConVar.
										 eQueryCvarValueStatus_CvarProtected = 3  // The cvar was marked with FCVAR_SERVER_CAN_NOT_QUERY, so the server is
																				  // not allowed to have its value.
} EQueryCvarValueStatus;

//-----------------------------------------------------------------------------
// Command tokenizer
//-----------------------------------------------------------------------------
struct characterset_t
{
	char set[256];
};

class CCommand
{
public:
	CCommand();
	CCommand(int nArgC, const char** ppArgV);
	void Reset();

	int ArgC() const;
	const char** ArgV() const;
	const char* ArgS() const;  // All args that occur after the 0th arg, in string form
	const char* GetCommandString() const;  // The entire command in string form, including the 0th arg
	const char* operator[](int nIndex) const;  // Gets at arguments
	const char* Arg(int nIndex) const;  // Gets at arguments

	// Helper functions to parse arguments to commands.
	const char* FindArg(const char* pName) const;
	int FindArgInt(const char* pName, int nDefaultVal) const;

	static int MaxCommandLength();
	static characterset_t* DefaultBreakSet();

private:
	enum
	{
		COMMAND_MAX_ARGC = 64,
		COMMAND_MAX_LENGTH = 512,
	};

	int m_nArgc;
	int m_nArgv0Size;
	char m_pArgSBuffer[COMMAND_MAX_LENGTH];
	char m_pArgvBuffer[COMMAND_MAX_LENGTH];
	const char* m_ppArgv[COMMAND_MAX_ARGC];
};

inline int CCommand::MaxCommandLength() { return COMMAND_MAX_LENGTH - 1; }

inline int CCommand::ArgC() const { return m_nArgc; }

inline const char** CCommand::ArgV() const { return m_nArgc ? (const char**)m_ppArgv : NULL; }

inline const char* CCommand::ArgS() const { return m_nArgv0Size ? &m_pArgSBuffer[m_nArgv0Size] : ""; }

inline const char* CCommand::GetCommandString() const { return m_nArgc ? m_pArgSBuffer : ""; }

inline const char* CCommand::Arg(int nIndex) const
{
	// FIXME: Many command handlers appear to not be particularly careful
	// about checking for valid argc range. For now, we're going to
	// do the extra check and return an empty string if it's out of range
	if (nIndex < 0 || nIndex >= m_nArgc)
		return "";
	return m_ppArgv[nIndex];
}

inline const char* CCommand::operator[](int nIndex) const { return Arg(nIndex); }

struct edict_t;
class KeyValues;

typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t byte;
typedef void* Pointer;
typedef unsigned char BYTE;
typedef int QueryCvarCookie_t;

template < typename T = Pointer >
bool IsPointer(T pP)
{
	return reinterpret_cast<Pointer>(pP) != nullptr;
}

template < typename T = Pointer * >
FORCEINLINE auto sub_GetVTable(Pointer pAddress) -> T
{
	return *reinterpret_cast<T*>(pAddress);
}

typedef void* (*CreateInterfaceFn)(const char* pName, int* pReturnCode);
typedef void* (*InstantiateInterfaceFn)();

#ifndef WIN32
#	define DLL_EXPORT extern "C" __attribute__( ( visibility( "default" ) ) )
#	define DLL_IMPORT extern "C"

#	define DLL_CLASS_EXPORT __attribute__( ( visibility( "default" ) ) )
#	define DLL_CLASS_IMPORT

#	define DLL_GLOBAL_EXPORT extern __attribute( ( visibility( "default" ) ) )
#	define DLL_GLOBAL_IMPORT extern

#	define DLL_LOCAL __attribute__( ( visibility( "hidden" ) ) )
#else
#define  DLL_EXPORT   extern "C" __declspec( dllexport )
#define  DLL_IMPORT   extern "C" __declspec( dllimport )
// Can't use extern "C" when DLL exporting a class
#define  DLL_CLASS_EXPORT   __declspec( dllexport )
#define  DLL_CLASS_IMPORT   __declspec( dllimport )

// Can't use extern "C" when DLL exporting a global
#define  DLL_GLOBAL_EXPORT   extern __declspec( dllexport )
#define  DLL_GLOBAL_IMPORT   extern __declspec( dllimport )
#endif

// Use this to expose an interface that can have multiple instances.
// e.g.:
// EXPOSE_INTERFACE( CInterfaceImp, IInterface, "MyInterface001" )
// This will expose a class called CInterfaceImp that implements IInterface (a
// pure class) clients can receive a pointer to this class by calling
// CreateInterface( "MyInterface001" )
//
// In practice, the shared header file defines the interface (IInterface) and
// version name ("MyInterface001") so that each component can use these
// names/vtables to communicate
//
// A single class can support multiple interfaces through multiple inheritance
//
// Use this if you want to write the factory function.
#	if !defined( _STATIC_LINKED ) || !defined( _SUBSYSTEM )
#		define EXPOSE_INTERFACE_FN( functionName, interfaceName, versionName ) \
			static InterfaceReg __g_Create##interfaceName##_reg( functionName, versionName );
#	else
#		define EXPOSE_INTERFACE_FN( functionName, interfaceName, versionName )                   \
			namespace _SUBSYSTEM                                                                  \
			{                                                                                     \
				static InterfaceReg __g_Create##interfaceName##_reg( functionName, versionName ); \
			}
#	endif

#	if !defined( _STATIC_LINKED ) || !defined( _SUBSYSTEM )
#		define EXPOSE_INTERFACE( className, interfaceName, versionName )                                             \
			static void* __Create##className##_interface() { return static_cast< interfaceName* >( new className ); } \
			static InterfaceReg __g_Create##className##_reg( __Create##className##_interface, versionName );
#	else
#		define EXPOSE_INTERFACE( className, interfaceName, versionName )                                                 \
			namespace _SUBSYSTEM                                                                                          \
			{                                                                                                             \
				static void* __Create##className##_interface() { return static_cast< interfaceName* >( new className ); } \
				static InterfaceReg __g_Create##className##_reg( __Create##className##_interface, versionName );          \
			}
#	endif

// Use this to expose a singleton interface with a global variable you've
// created.
#	if !defined( _STATIC_LINKED ) || !defined( _SUBSYSTEM )
#		define EXPOSE_SINGLE_INTERFACE_GLOBALVAR_WITH_NAMESPACE( className, interfaceNamespace, interfaceName, versionName, globalVarName ) \
			static void* __Create##className##interfaceName##_interface()                                                                    \
			{                                                                                                                                \
				return static_cast< interfaceNamespace interfaceName* >( &globalVarName );                                                   \
			}                                                                                                                                \
			static InterfaceReg __g_Create##className##interfaceName##_reg( __Create##className##interfaceName##_interface, versionName );
#	else
#		define EXPOSE_SINGLE_INTERFACE_GLOBALVAR_WITH_NAMESPACE( className, interfaceNamespace, interfaceName, versionName, globalVarName )   \
			namespace _SUBSYSTEM                                                                                                               \
			{                                                                                                                                  \
				static void* __Create##className##interfaceName##_interface()                                                                  \
				{                                                                                                                              \
					return static_cast< interfaceNamespace interfaceName* >( &globalVarName );                                                 \
				}                                                                                                                              \
				static InterfaceReg __g_Create##className##interfaceName##_reg( __Create##className##interfaceName##_interface, versionName ); \
			}
#	endif

#	define EXPOSE_SINGLE_INTERFACE_GLOBALVAR( className, interfaceName, versionName, globalVarName ) \
		EXPOSE_SINGLE_INTERFACE_GLOBALVAR_WITH_NAMESPACE( className, , interfaceName, versionName, globalVarName )

// Use this to expose a singleton interface. This creates the global variable
// for you automatically.
#	if !defined( _STATIC_LINKED ) || !defined( _SUBSYSTEM )
#		define EXPOSE_SINGLE_INTERFACE( className, interfaceName, versionName ) \
			static className __g_##className##_singleton;                        \
			EXPOSE_SINGLE_INTERFACE_GLOBALVAR( className, interfaceName, versionName, __g_##className##_singleton )
#	else
#		define EXPOSE_SINGLE_INTERFACE( className, interfaceName, versionName ) \
			namespace _SUBSYSTEM                                                 \
			{                                                                    \
				static className __g_##className##_singleton;                    \
			}                                                                    \
			EXPOSE_SINGLE_INTERFACE_GLOBALVAR( className, interfaceName, versionName, __g_##className##_singleton )
#	endif

// Used internally to register classes.
class InterfaceReg
{
public:
	InterfaceReg(InstantiateInterfaceFn fn, const char* pName);

public:
	InstantiateInterfaceFn m_CreateFn;
	const char* m_pName;

	InterfaceReg* m_pNext;  // For the global list.
	static InterfaceReg* s_pInterfaceRegs;
};

class IBaseInterface
{
public:
	virtual ~IBaseInterface() {}
};

class IGameEventListener
{
public:
	virtual ~IGameEventListener(void) {};

	// FireEvent is called by EventManager if event just occured
	// KeyValue memory will be freed by manager if not needed anymore
	virtual void FireGameEvent(KeyValues* event) = 0;
};

class IServerPluginCallbacks
{
public:
	// Initialize the plugin to run
	// Return false if there is an error during startup.
	virtual bool Load(CreateInterfaceFn interfaceFactory, CreateInterfaceFn gameServerFactory) = 0;

	// Called when the plugin should be shutdown
	virtual void Unload(void) = 0;

	// called when a plugins execution is stopped but the plugin is not unloaded
	virtual void Pause(void) = 0;

	// called when a plugin should start executing again (sometime after a
	// Pause() call)
	virtual void UnPause(void) = 0;

	// Returns string describing current plugin.  e.g., Admin-Mod.
	virtual const char* GetPluginDescription(void) = 0;

	// Called any time a new level is started (after GameInit() also on level
	// transitions within a game)
	virtual void LevelInit(char const* pMapName) = 0;

	// The server is about to activate
	virtual void ServerActivate(edict_t* pEdictList, int edictCount, int clientMax) = 0;

	// The server should run physics/think on all edicts
	virtual void GameFrame(bool simulating) = 0;

	// Called when a level is shutdown (including changing levels)
	virtual void LevelShutdown(void) = 0;

	// Client is going active
	virtual void ClientActive(edict_t* pEntity) = 0;

	// Client is disconnecting from server
	virtual void ClientDisconnect(edict_t* pEntity) = 0;

	// Client is connected and should be put in the game
	virtual void ClientPutInServer(edict_t* pEntity, char const* playername) = 0;

	// Sets the client index for the client who typed the command into their
	// console
	virtual void SetCommandClient(int index) = 0;

	// A player changed one/several replicated cvars (name etc)
	virtual void ClientSettingsChanged(edict_t* pEdict) = 0;

	// Client is connecting to server ( set retVal to false to reject the
	// connection )
	//	You can specify a rejection message by writing it into reject
	virtual PLUGIN_RESULT
		ClientConnect(bool* bAllowConnect, edict_t* pEntity, const char* pszName, const char* pszAddress, char* reject, int maxrejectlen) = 0;

	// The client has typed a command at the console
	virtual PLUGIN_RESULT ClientCommand(edict_t* pEntity, const CCommand& args) = 0;

	// A user has had their network id setup and validated
	virtual PLUGIN_RESULT NetworkIDValidated(const char* pszUserName, const char* pszNetworkID) = 0;

	// This is called when a query from
	// IServerPluginHelpers::StartQueryCvarValue is finished. iCookie is the
	// value returned by IServerPluginHelpers::StartQueryCvarValue. Added with
	// version 2 of the interface.
	virtual void OnQueryCvarValueFinished(QueryCvarCookie_t iCookie,
		edict_t* pPlayerEntity,
		EQueryCvarValueStatus eStatus,
		const char* pCvarName,
		const char* pCvarValue) = 0;

	// added with version 3 of the interface.
	virtual void OnEdictAllocated(edict_t* edict) = 0;
	virtual void OnEdictFreed(const edict_t* edict) = 0;
};

class CClipToolPlugin : public IServerPluginCallbacks, public IGameEventListener
{
	// IServerPluginCallbacks methods
public:
	CClipToolPlugin() { m_iClientCommandIndex = 0; }

	~CClipToolPlugin() {}

	virtual bool Load(CreateInterfaceFn interfaceFactory, CreateInterfaceFn gameServerFactory);
	virtual void Unload(void);
	virtual void Pause(void) {};
	virtual void UnPause(void) {};
	virtual const char* GetPluginDescription(void) { return "Clip Tool"; };
	virtual void LevelInit(char const* pMapName);
	virtual void ServerActivate(edict_t* pEdictList, int edictCount, int clientMax) {};
	virtual void GameFrame(bool simulating) {};
	virtual void LevelShutdown(void) {};
	virtual void ClientActive(edict_t* pEntity) {};
	virtual void ClientFullyConnect(edict_t* pEntity) {};
	virtual void ClientDisconnect(edict_t* pEntity) {};
	virtual void ClientPutInServer(edict_t* pEntity, char const* playername) {};
	virtual void SetCommandClient(int index) {};
	virtual void ClientSettingsChanged(edict_t* pEdict) {};
	virtual PLUGIN_RESULT
		ClientConnect(bool* bAllowConnect, edict_t* pEntity, const char* pszName, const char* pszAddress, char* reject, int maxrejectlen)
	{
		return PLUGIN_CONTINUE;
	};
	virtual PLUGIN_RESULT ClientCommand(edict_t* pEntity, const CCommand& args);

	virtual PLUGIN_RESULT NetworkIDValidated(const char* pszUserName, const char* pszNetworkID) { return PLUGIN_CONTINUE; };
	virtual void OnQueryCvarValueFinished(QueryCvarCookie_t iCookie,
		edict_t* pPlayerEntity,
		EQueryCvarValueStatus eStatus,
		const char* pCvarName,
		const char* pCvarValue) {};
	virtual void OnEdictAllocated(edict_t* edict) {};
	virtual void OnEdictFreed(const edict_t* edict) {};
	virtual void FireGameEvent(KeyValues* event) {};
	virtual bool BNetworkCryptKeyCheckRequired(uint32 unFromIP,
		uint16 usFromPort,
		uint32 unAccountIdProvidedByClient,
		bool bClientWantsToUseCryptKey)
	{
		return true;
	}
	virtual bool BNetworkCryptKeyValidate(uint32 unFromIP,
		uint16 usFromPort,
		uint32 unAccountIdProvidedByClient,
		int nEncryptionKeyIndexFromClient,
		int numEncryptedBytesFromClient,
		byte* pbEncryptedBufferFromClient,
		byte* pbPlainTextKeyForNetchan)
	{
		return true;
	}
	int GetCommandIndex() { return m_iClientCommandIndex; }

private:
	int m_iClientCommandIndex;
};

#	define INTERFACEVERSION_VENGINESERVER "VEngineServer023"

class IVEngineServer
{
public:
	// Tell engine to change level ( "changelevel s1\n" or "changelevel2 s1
	// s2\n" )
	virtual void ChangeLevel(const char* s1, const char* s2) = 0;

	// Ask engine whether the specified map is a valid map file (exists and has
	// valid version number).
	virtual int IsMapValid(const char* filename) = 0;

	// Is this a dedicated server?
	virtual bool IsDedicatedServer(void) = 0;

	// Is in Hammer editing mode?
	virtual int IsInEditMode(void) = 0;

	// Add to the server/client lookup/precache table, the specified string is
	// given a unique index NOTE: The indices for PrecacheModel are 1 based
	//  a 0 returned from those methods indicates the model or sound was not
	//  correctly precached
	// However, generic and decal are 0 based
	// If preload is specified, the file is loaded into the server/client's
	// cache memory before level startup, otherwise
	//  it'll only load when actually used (which can cause a disk i/o hitch if
	//  it occurs during play of a level).
	virtual int PrecacheModel(const char* s, bool preload = false) = 0;
	virtual int PrecacheSentenceFile(const char* s, bool preload = false) = 0;
	virtual int PrecacheDecal(const char* name, bool preload = false) = 0;
	virtual int PrecacheGeneric(const char* s, bool preload = false) = 0;

	// Check's if the name is precached, but doesn't actually precache the name
	// if not...
	virtual bool IsModelPrecached(char const* s) const = 0;
	virtual bool IsDecalPrecached(char const* s) const = 0;
	virtual bool IsGenericPrecached(char const* s) const = 0;

	// Note that sounds are precached using the IEngineSound interface

	// Special purpose PVS checking
	// Get the cluster # for the specified position
	virtual int GetClusterForOrigin(const Vector& org) = 0;
	// Get the PVS bits for a specified cluster and copy the bits into
	// outputpvs.  Returns the number of bytes needed to pack the PVS
	virtual int GetPVSForCluster(int cluster, int outputpvslength, unsigned char* outputpvs) = 0;
	// Check whether the specified origin is inside the specified PVS
	virtual bool CheckOriginInPVS(const Vector& org, const unsigned char* checkpvs, int checkpvssize) = 0;
	// Check whether the specified worldspace bounding box is inside the
	// specified PVS
	virtual bool CheckBoxInPVS(const Vector& mins, const Vector& maxs, const unsigned char* checkpvs, int checkpvssize) = 0;

	// Returns the server assigned userid for this player.  Useful for logging
	// frags, etc.
	//  returns -1 if the edict couldn't be found in the list of players.
	virtual int GetPlayerUserId(const edict_t* e) = 0;
	virtual const char* GetPlayerNetworkIDString(const edict_t* e) = 0;

	// Return the current number of used edict slots
	virtual int GetEntityCount(void) = 0;
	// Given an edict, returns the entity index
	virtual int IndexOfEdict(const edict_t* pEdict) = 0;
	// Given and entity index, returns the corresponding edict pointer
	virtual edict_t* PEntityOfEntIndex(int iEntIndex) = 0;

	// Get stats info interface for a client netchannel
	virtual void* GetPlayerNetInfo(int playerIndex) = 0;

	// Allocate space for string and return index/offset of string in global
	// string list If iForceEdictIndex is not -1, then it will return the edict
	// with that index. If that edict index is already used, it'll return null.
	virtual edict_t* CreateEdict(int iForceEdictIndex = -1) = 0;
	// Remove the specified edict and place back into the free edict list
	virtual void RemoveEdict(edict_t* e) = 0;

	// Memory allocation for entity class data
	virtual void* PvAllocEntPrivateData(long cb) = 0;
	virtual void FreeEntPrivateData(void* pEntity) = 0;

	// Save/restore uses a special memory allocator (which zeroes newly
	// allocated memory, etc.)
	virtual void* SaveAllocMemory(size_t num, size_t size) = 0;
	virtual void SaveFreeMemory(void* pSaveMem) = 0;

	// Emit an ambient sound associated with the specified entity
	virtual void
		EmitAmbientSound(int entindex, const Vector& pos, const char* samp, float vol, int soundlevel, int fFlags, int pitch, float delay = 0.0f) = 0;

	// Fade out the client's volume level toward silence (or fadePercent)
	virtual void FadeClientVolume(const edict_t* pEdict, float fadePercent, float fadeOutSeconds, float holdTime, float fadeInSeconds) = 0;

	// Sentences / sentence groups
	virtual int SentenceGroupPick(int groupIndex, char* name, int nameBufLen) = 0;
	virtual int SentenceGroupPickSequential(int groupIndex, char* name, int nameBufLen, int sentenceIndex, int reset) = 0;
	virtual int SentenceIndexFromName(const char* pSentenceName) = 0;
	virtual const char* SentenceNameFromIndex(int sentenceIndex) = 0;
	virtual int SentenceGroupIndexFromName(const char* pGroupName) = 0;
	virtual const char* SentenceGroupNameFromIndex(int groupIndex) = 0;
	virtual float SentenceLength(int sentenceIndex) = 0;

	// Issue a command to the command parser as if it was typed at the server
	// console.
	virtual void ServerCommand(const char* str) = 0;
	// Execute any commands currently in the command parser immediately (instead
	// of once per frame)
	virtual void ServerExecute(void) = 0;
	// Issue the specified command to the specified client (mimics that client
	// typing the command at the console).
	virtual void ClientCommand(edict_t* pEdict, const char* szFmt, ...) = 0;

	// Set the lightstyle to the specified value and network the change to any
	// connected clients.  Note that val must not
	//  change place in memory (use MAKE_STRING) for anything that's not
	//  compiled into your mod.
	virtual void LightStyle(int style, const char* val) = 0;

	// Project a static decal onto the specified entity / model (for level
	// placed decals in the .bsp)
	virtual void StaticDecal(const Vector& originInEntitySpace, int decalIndex, int entityIndex, int modelIndex, bool lowpriority) = 0;

	// Given the current PVS(or PAS) and origin, determine which players should
	// hear/receive the message
	virtual void Message_DetermineMulticastRecipients(bool usepas, const Vector& origin, void* playerbits) = 0;

	// Begin a message from a server side entity to its client side counterpart
	// (func_breakable glass, e.g.)
	virtual void* EntityMessageBegin(int ent_index, void* ent_class, bool reliable) = 0;
	// Begin a usermessage from the server to the client .dll
	virtual void* UserMessageBegin(void* filter, int msg_type) = 0;
	// Finish the Entity or UserMessage and dispatch to network layer
	virtual void MessageEnd(void) = 0;

	// Print szMsg to the client console.
	virtual void ClientPrintf(edict_t* pEdict, const char* szMsg) = 0;

	// SINGLE PLAYER/LISTEN SERVER ONLY (just matching the client .dll api for
	// this) Prints the formatted string to the notification area of the screen
	// ( down the right hand edge
	//  numbered lines starting at position 0
	virtual void Con_NPrintf(int pos, const char* fmt, ...) = 0;
	// SINGLE PLAYER/LISTEN SERVER ONLY(just matching the client .dll api for
	// this) Similar to Con_NPrintf, but allows specifying custom text color and
	// duration information
	virtual void Con_NXPrintf(const struct con_nprint_s* info, const char* fmt, ...) = 0;

	// Change a specified player's "view entity" (i.e., use the view entity
	// position/orientation for rendering the client view)
	virtual void SetView(const edict_t* pClient, const edict_t* pViewent) = 0;

	// Get a high precision timer for doing profiling work
	virtual float Time(void) = 0;

	// Set the player's crosshair angle
	virtual void CrosshairAngle(const edict_t* pClient, float pitch, float yaw) = 0;

	// Get the current game directory (hl2, tf2, hl1, cstrike, etc.)
	virtual void GetGameDir(char* szGetGameDir, int maxlength) = 0;

	// Used by AI node graph code to determine if .bsp and .ain files are out of
	// date
	virtual int CompareFileTime(const char* filename1, const char* filename2, int* iCompare) = 0;

	// Locks/unlocks the network string tables (.e.g, when adding bots to
	// server, this needs to happen). Be sure to reset the lock after executing
	// your code!!!
	virtual bool LockNetworkStringTables(bool lock) = 0;

	// Create a bot with the given name.  Returns NULL if fake client can't be
	// created
	virtual edict_t* CreateFakeClient(const char* netname) = 0;

	// Get a convar keyvalue for s specified client
	virtual const char* GetClientConVarValue(int clientIndex, const char* name) = 0;

	// Parse a token from a file
	virtual const char* ParseFile(const char* data, char* token, int maxlen) = 0;
	// Copies a file
	virtual bool CopyFile(const char* source, const char* destination) = 0;

	// Reset the pvs, pvssize is the size in bytes of the buffer pointed to by
	// pvs. This should be called right before any calls to AddOriginToPVS
	virtual void ResetPVS(byte* pvs, int pvssize) = 0;
	// Merge the pvs bits into the current accumulated pvs based on the
	// specified origin ( not that each pvs origin has an 8 world unit fudge
	// factor )
	virtual void AddOriginToPVS(const Vector& origin) = 0;

	// Mark a specified area portal as open/closed.
	// Use SetAreaPortalStates if you want to set a bunch of them at a time.
	virtual void SetAreaPortalState(int portalNumber, int isOpen) = 0;

	// Queue a temp entity for transmission
	virtual void PlaybackTempEntity(void* filter, float delay, const void* pSender, const void* pST, int classID) = 0;
	// Given a node number and the specified PVS, return with the node is in the
	// PVS
	virtual int CheckHeadnodeVisible(int nodenum, const byte* pvs, int vissize) = 0;
	// Using area bits, cheeck whether area1 flows into area2 and vice versa
	// (depends on area portal state)
	virtual int CheckAreasConnected(int area1, int area2) = 0;
	// Given an origin, determine which area index the origin is within
	virtual int GetArea(const Vector& origin) = 0;
	// Get area portal bit set
	virtual void GetAreaBits(int area, unsigned char* bits, int buflen) = 0;
	// Given a view origin (which tells us the area to start looking in) and a
	// portal key, fill in the plane that leads out of this area (it points into
	// whatever area it leads to).
	virtual bool GetAreaPortalPlane(Vector const& vViewOrigin, int portalKey, void* pPlane) = 0;

	// Save/restore wrapper - FIXME:  At some point we should move this to it's
	// own interface
	virtual bool LoadGameState(char const* pMapName, bool createPlayers) = 0;
	virtual void LoadAdjacentEnts(const char* pOldLevel, const char* pLandmarkName) = 0;
	virtual void ClearSaveDir() = 0;

	// Get the pristine map entity lump string.  (e.g., used by CS to reload the
	// map entities when restarting a round.)
	virtual const char* GetMapEntitiesString() = 0;

	// Text message system -- lookup the text message of the specified name
	virtual void* TextMessageGet(const char* pName) = 0;

	// Print a message to the server log file
	virtual void LogPrint(const char* msg) = 0;

	// Builds PVS information for an entity
	virtual void BuildEntityClusterList(edict_t* pEdict, void* pPVSInfo) = 0;

	// A solid entity moved, update spatial partition
	virtual void SolidMoved(edict_t* pSolidEnt, void* pSolidCollide, const Vector* pPrevAbsOrigin, bool testSurroundingBoundsOnly) = 0;
	// A trigger entity moved, update spatial partition
	virtual void TriggerMoved(edict_t* pTriggerEnt, bool testSurroundingBoundsOnly) = 0;

	// Create/destroy a custom spatial partition
	virtual void* CreateSpatialPartition(const Vector& worldmin, const Vector& worldmax) = 0;
	virtual void DestroySpatialPartition(void*) = 0;

	// Draw the brush geometry in the map into the scratch pad.
	// Flags is currently unused.
	virtual void DrawMapToScratchPad(void* pPad, unsigned long iFlags) = 0;

	// This returns which entities, to the best of the server's knowledge, the
	// client currently knows about. This is really which entities were in the
	// snapshot that this client last acked. This returns a bit vector with one
	// bit for each entity.
	//
	// USE WITH CARE. Whatever tick the client is really currently on is subject
	// to timing and ordering differences, so you should account for about a
	// quarter-second discrepancy in here. Also, this will return NULL if the
	// client doesn't exist or if this client hasn't acked any frames yet.
	//
	// iClientIndex is the CLIENT index, so if you use pPlayer->entindex(),
	// subtract 1.
	virtual const void* GetEntityTransmitBitsForClient(int iClientIndex) = 0;

	// Is the game paused?
	virtual bool IsPaused() = 0;

	// Marks the filename for consistency checking.  This should be called after
	// precaching the file.
	virtual void ForceExactFile(const char* s) = 0;
	virtual void ForceModelBounds(const char* s, const Vector& mins, const Vector& maxs) = 0;
	virtual void ClearSaveDirAfterClientLoad() = 0;

	// Sets a USERINFO client ConVar for a fakeclient
	virtual void SetFakeClientConVarValue(edict_t* pEntity, const char* cvar, const char* value) = 0;

	// Marks the material (vmt file) for consistency checking.  If the client
	// and server have different contents for the file, the client's vmt can
	// only use the VertexLitGeneric shader, and can only contain $baseTexture
	// and $bumpmap vars.
	virtual void ForceSimpleMaterial(const char* s) = 0;

	// Is the engine in Commentary mode?
	virtual int IsInCommentaryMode(void) = 0;

	// Mark some area portals as open/closed. It's more efficient to use this
	// than a bunch of individual SetAreaPortalState calls.
	virtual void SetAreaPortalStates(const int* portalNumbers, const int* isOpen, int nPortals) = 0;

	// Called when relevant edict state flags change.
	virtual void NotifyEdictFlagsChange(int iEdict) = 0;

	// Only valid during CheckTransmit. Also, only the PVS, networked areas, and
	// m_pTransmitInfo are valid in the returned strucutre.
	virtual const void* GetPrevCheckTransmitInfo(edict_t* pPlayerEdict) = 0;

	virtual void* GetSharedEdictChangeInfo() = 0;

	// Tells the engine we can immdiately re-use all edict indices
	// even though we may not have waited enough time
	virtual void AllowImmediateEdictReuse() = 0;

	// Returns true if the engine is an internal build. i.e. is using the
	// internal bugreporter.
	virtual bool IsInternalBuild(void) = 0;

	virtual void* GetChangeAccessor(const edict_t* pEdict) = 0;

	// Name of most recently load .sav file
	virtual char const* GetMostRecentlyLoadedFileName() = 0;
	virtual char const* GetSaveFileName() = 0;

	// Matchmaking
	virtual void MultiplayerEndGame() = 0;
	virtual void ChangeTeam(const char* pTeamName) = 0;

	// Cleans up the cluster list
	virtual void CleanUpEntityClusterList(void* pPVSInfo) = 0;

	virtual void SetAchievementMgr(void* pAchievementMgr) = 0;
	virtual void* GetAchievementMgr() = 0;

	virtual int GetAppID() = 0;

	virtual bool IsLowViolence() = 0;

	// Call this to find out the value of a cvar on the client.
	//
	// It is an asynchronous query, and it will call
	// IServerGameDLL::OnQueryCvarValueFinished when the value comes in from the
	// client.
	//
	// Store the return value if you want to match this specific query to the
	// OnQueryCvarValueFinished call. Returns InvalidQueryCvarCookie if the
	// entity is invalid.
	virtual QueryCvarCookie_t StartQueryCvarValue(edict_t* pPlayerEntity, const char* pName) = 0;

	virtual void InsertServerCommand(const char* str) = 0;

	// Fill in the player info structure for the specified player index (name,
	// model, etc.)
	virtual bool GetPlayerInfo(int ent_num, void* pinfo) = 0;

	// Returns true if this client has been fully authenticated by Steam
	virtual bool IsClientFullyAuthenticated(edict_t* pEdict) = 0;

	// This makes the host run 1 tick per frame instead of checking the system
	// timer to see how many ticks to run in a certain frame. i.e. it does the
	// same thing timedemo does.
	virtual void SetDedicatedServerBenchmarkMode(bool bBenchmarkMode) = 0;

	// Methods to set/get a gamestats data container so client & server running
	// in same process can send combined data
	virtual void SetGamestatsData(void* pGamestatsData) = 0;
	virtual void* GetGamestatsData() = 0;

	// Returns the SteamID of the specified player. It'll be NULL if the player
	// hasn't authenticated yet.
	virtual const void* GetClientSteamID(edict_t* pPlayerEdict) = 0;

	// Returns the SteamID of the game server
	virtual const void* GetGameServerSteamID() = 0;

	// Send a client command keyvalues
	// keyvalues are deleted inside the function
	virtual void ClientCommandKeyValues(edict_t* pEdict, KeyValues* pCommand) = 0;

	// Returns the SteamID of the specified player. It'll be NULL if the player
	// hasn't authenticated yet.
	virtual const void* GetClientSteamIDByPlayerIndex(int entnum) = 0;
	// Gets a list of all clusters' bounds.  Returns total number of clusters.
	virtual int GetClusterCount() = 0;
	virtual int GetAllClusterBounds(void* pBBoxList, int maxBBox) = 0;

	// Create a bot with the given name.  Returns NULL if fake client can't be
	// created
	virtual edict_t* CreateFakeClientEx(const char* netname, bool bReportFakeClient = true) = 0;

	// Server version from the steam.inf, this will be compared to the GC
	// version
	virtual int GetServerVersion() const = 0;

	// Get sv.GetTime()
	virtual float GetServerTime() const = 0;

	// Exposed for server plugin authors
	virtual void* GetIServer() = 0;

	virtual bool IsPlayerNameLocked(const edict_t* pEdict) = 0;
	virtual bool CanPlayerChangeName(const edict_t* pEdict) = 0;

	// Find the canonical name of a map, given a partial or non-canonical map
	// name. Except in the case of an exact match, pMapName is updated to the
	// canonical name of the match. NOTE That this is subject to the same
	// limitation as ServerGameDLL::CanProvideLevel -- This is non-blocking, so
	// it
	//      is possible that blocking ServerGameDLL::PrepareLevelResources call
	//      may be able to pull a better match than is immediately available to
	//      this call (e.g. blocking lookups of cloud maps)
	enum eFindMapResult
	{
		// A direct match for this name was found
		eFindMap_Found,
		// No match for this map name could be found.
		eFindMap_NotFound,
		// A fuzzy match for this mapname was found and pMapName was updated to
		// the full name.
		// Ex: cp_dust -> cp_dustbowl
		eFindMap_FuzzyMatch,
		// A match for this map name was found, and the map name was updated to
		// the canonical version of the
		// name.
		// Ex: workshop/1234 -> workshop/cp_qualified_name.ugc1234
		eFindMap_NonCanonical,
		// No currently available match for this map name could be found, but it
		// may be possible to load ( see caveat
		// about PrepareLevelResources above )
		eFindMap_PossiblyAvailable
	};
	virtual eFindMapResult FindMap( /* in/out */ char* pMapName, int nMapNameMax) = 0;
};

#endif
